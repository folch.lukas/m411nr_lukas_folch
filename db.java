import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
/**
 *
 * @author lukas
 */

public class db {
    private Connection conn;
    public db() {
        String  baseurl = "jdbc:mysql://localhost/pr�fung_db";     
        String  user = "root";
        String  password = "";
        
        try {
            conn = DriverManager.getConnection(baseurl, user, password);
        } catch (Exception e) {
            System.err.println(e);
            System.exit(0);
        }
    }
    
    
    public ArrayList<String> selectName() {
        ArrayList<String> nlist = new ArrayList();
        Statement sqlStatement;
        ResultSet result;
        try {
            sqlStatement = conn.createStatement();
            sqlStatement.execute("SELECT * FROM NAME"); 
            result = sqlStatement.getResultSet(); 
            while (result.next()) {
                nlist.add( result.getString("name").toLowerCase() );
            }
        } catch (Exception e) {
            System.out.println(e);
        }       
        return nlist;
    }
    
    
    public ArrayList<String> selectVorname() {
        ArrayList<String> vlist = new ArrayList();
        Statement sqlStatement;
        ResultSet result;
        try {
            sqlStatement = conn.createStatement();
            sqlStatement.execute("SELECT * FROM VORNAME"); 
            result = sqlStatement.getResultSet(); 
            while (result.next()) {
                vlist.add( result.getString("vorname").toLowerCase() );
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return vlist;
    }
    
    
    public ArrayList<String> selectFarbe() {
        ArrayList<String> flist = new ArrayList();
        Statement sqlStatement;
        ResultSet result;
        try {
            sqlStatement = conn.createStatement();
            sqlStatement.execute("SELECT * FROM FARBE"); 
            result = sqlStatement.getResultSet(); 
            while (result.next()) {
                flist.add(result.getString("fid"));
            }
        } catch (Exception e) {
            System.out.println(e);
        }       
        return flist;
    }
    
    
    public HashMap<String, Integer> addPerson(ArrayList<String> name, ArrayList<String> vorname, ArrayList<String> farbe, int i) {
        Statement sqlStatement;
        ResultSet result;
        HashMap<String, Integer> HaschName = new HashMap<String, Integer>();
        int numValues = 1;
        for(int x = 0; x < i; x++){
            int randNameKey = (int)(Math.random()*name.size());
            int randVornameKey = (int)(Math.random()*vorname.size());
            int randFarbeKey = (int)(Math.random()*farbe.size());
            String randName = name.get(randNameKey);
            String randVorname = vorname.get(randVornameKey);
            String randFarbe = farbe.get(randFarbeKey);
            try {
                sqlStatement = conn.createStatement();
                sqlStatement.execute("INSERT INTO personen (name, vorname, fid) VALUES ('" + randName + ("' ,'") + randVorname + ("', '1')"));
            } catch (Exception e) {
                System.out.println(e);
            }
            if(HaschName.get(randName) == null) {
                HaschName.put(randName, 1);
            } else {
                HaschName.put(randName, HaschName.get(randName) + 1);
            }
        }
        return HaschName;
    }
}