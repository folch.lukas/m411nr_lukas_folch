import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
/**
 *
 * @author lukas
 */
public class main {
    public static void main( String args[] ) {
        db dbhandle = new db();
        ArrayList<String> nlist = dbhandle.selectName();
        ArrayList<String> vlist = dbhandle.selectVorname();
        ArrayList<String> flist = dbhandle.selectFarbe();
        int x;
        Scanner scanner = new Scanner (System.in);
        
        
        System.out.print("n: ");
        x = scanner.nextInt();
        int min = x;
        int max = 0;
        int diff;
        
        /*
        for (String n: nlist) {
            System.out.println(n);
        }
        
        for (String v: vlist) {
            System.out.println(v);
        }
        
        for(String f: flist) {
            System.out.println(f);
        }
        */
        
        HashMap<String, Integer> Hasch = dbhandle.addPerson(nlist, vlist, flist, x);
        
        for(String i: Hasch.keySet()){
            System.out.println(i + ": " + Hasch.get(i));
            if(Hasch.get(i) < min){
                min = Hasch.get(i);
            }
            if(Hasch.get(i) > max) {
                max = Hasch.get(i);
            }
        }
        diff = max - min;
        System.out.println("Differenz: " + diff);
    }
}